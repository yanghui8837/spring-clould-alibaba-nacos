package cn.myfreecloud.service2.service;

import cn.myfreecloud.api.ProviderService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@org.apache.dubbo.config.annotation.Service
public class ProviderServiceImpl implements ProviderService {

    @Override
    public String service() {
        log.info("=====Dubbo ProviderService 被调用");

        return "provider2 invoke";
    }
}
