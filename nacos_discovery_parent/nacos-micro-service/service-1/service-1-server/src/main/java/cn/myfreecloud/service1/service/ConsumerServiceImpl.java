package cn.myfreecloud.service1.service;

import cn.myfreecloud.api.ConsumerService;
import cn.myfreecloud.api.ProviderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;

// 注解表示
// 此类的方法将来要暴露为Dubbo接口
@Slf4j
@RefreshScope
@org.apache.dubbo.config.annotation.Service
public class ConsumerServiceImpl implements ConsumerService {

    @org.apache.dubbo.config.annotation.Reference
    ProviderService providerService;

    @Value("${spring.test}")
    private String context;


    // Dubbo接口的内容
    @Override
    public String service() {
        log.info("=====Dubbo ConsumerService 被调用,context:{}",context);
        // service1 调用 service2
        String service = providerService.service();
        log.info("=====Dubbo providerService 被调用,context:{}",context);
        return "Consumer1 invoke" + service;
    }
}
