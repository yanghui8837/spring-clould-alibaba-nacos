package cn.myfreecloud.controller;

import cn.myfreecloud.api.ConsumerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: zhangyang
 * @date: 2020/5/8 23:36
 * @description:
 */
@RefreshScope
@RestController
public class ApplicationController {

    //注入service 基于dubbo协议
    @org.apache.dubbo.config.annotation.Reference
    ConsumerService consumerService;

    @Value("${spring.test}")
    private String test;

    @GetMapping("/service")
    public String service() {
        String service = "";
//         service = consumerService == null ? "" :consumerService.service();

        return "test:" + test+":"+service;
    }


}
