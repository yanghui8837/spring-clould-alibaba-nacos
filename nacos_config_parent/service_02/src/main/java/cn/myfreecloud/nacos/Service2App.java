package cn.myfreecloud.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: zhangyang
 * @date: 2020/5/8 14:47
 * @description:
 */
@SpringBootApplication
public class Service2App {
    public static void main(String[] args) {
        SpringApplication.run(Service2App.class, args);
    }
}
